const express = require('express');
const router = express.Router();
const companies = require('../data/companies');

// Get all companies
router.get('/', (req, res) => {
  res.json(companies);
});

module.exports = router;