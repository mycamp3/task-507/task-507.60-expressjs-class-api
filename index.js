const express = require('express');
const app = express();
const companyRouter = require('./routes/companies');

app.use(express.json()); // For parsing application/json

app.use('/companies', companyRouter);

const PORT = process.env.PORT || 8000;
app.listen(PORT, () => {
  console.log(`App is running on port ${PORT}`);
});