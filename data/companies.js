const Company = require('../models/company');

const companies = [
  new Company(1, 'Alfreds Futterkiste', 'Maria Anders', 'Germany'),
  new Company(2, 'Centro comercial Moctezuma', 'Francisco Chang', 'Mexico'),
  new Company(3, 'Ernst Handel', 'Roland Mendel', 'Austria'),
  new Company(4, 'Island Trading', 'Helen Bennett', 'UK'),
  new Company(5, 'Laughing Bacchus Winecellars', 'Yoshi Tannamuri', 'Canada'),
  new Company(6, 'Magazzini Alimentari Riuniti', 'Giovanni Rovelli', 'Italy')
];

module.exports = companies;